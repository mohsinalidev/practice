<?php
use Eshop\Post;
use Eshop\User;
use Eshop\Role;
use Eshop\Country;
use Eshop\Photo;
use Eshop\Video;
use Eshop\Tag;
use Eshop\Taggable;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/test', 'HomeController@test');

Route::get('/sandbox', 'HomeController@sandbox');

Route::get('/ebay', 'SandboxController@test');

Route::get('/ebaytime', 'SandboxController@eBayTime');

Route::get('/ebaystore', 'SandboxController@ebaystore');

Route::get('/maps', 'SandboxController@geogram');

Route::get('/maps?{address}', 'SandboxController@geogram');

Route::post('/addrole', 'HomeController@addrole');

Route::get('/getreport', function () {
//    return(app_path());
    $uri = base_path('packages/amazon-mws/laravel-mohsinintazar/src/MarketplaceWebService/Samples/');
    return require $uri . ('GetReportSample.php');
//    return require base_path('/packages/amazon-mws/laravel-mohsinintazar/src/MarketplaceWebService/Samples/GetReportSample.php');
});

// Database Raw SQL Queries
/*
Route::get('/insert', function(){
    DB::insert('INSERT INTO posts(title, content) values(?, ?)', ['4- PHP with Laravel', '4- Laravel 5 is the best thing that has happened to PHP']);
});

Route::get('/read', function(){
    $result = DB::select('SELECT * FROM posts');

    foreach($result as $row){
        echo '<h1>'.$row->title.'</h1>';
        echo '<p>'.$row->content.'</p>';
    }
});

Route::get('/update', function(){
    $updated = DB::update('UPDATE posts SET title="2- Updated title" WHERE id = ?', [2]);
});
 * 
 */

// Eloquent ORM

Route::get('/read', function(){
    $posts = Post::all();
    foreach($posts as $post){
        echo '<h1>'.$post->title.'</h1>';
        echo '<p>'.$post->content.'<p>';
    }
});

Route::get('/find', function(){
    $post = Post::find(2);
    return $post->title;
});

Route::get('/findwhere', function(){
    $post = Post::where('id', 2)->orderBy('id','desc')->get();
    var_dump($post);
    return $post;
});

Route::get('/findmore', function(){
//    $post = Post::findOrFail(1);
    $post = Post::where('is_admin', '<', 50)->firstOrFail();
    return $post;
});

Route::get('/basicinsert', function(){
    $post = new Post();
    $post->title = "New Eloquent title insert";
    $post->content = "This is a new eloquent content";
    $post->save();
    return $post;
});

Route::get('/basicupdate', function(){
    $post = Post::find(2);
    $post->title = "New Eloquent title insert";
    $post->content = "This is a new eloquent content";
    $post->save();
    return $post;
});

//This method throws error or MassInsert Exception
Route::get('/create', function(){
    for($i=1; $i<20; $i++){
//    $post = Post::create(['title'=>"$i - Created it", 'content'=>"$i - This is the content from created method."]);
        $post = Post::create(['title'=> "$i - Title for post", 'content'=> "$i - Content for this post"]);
    }
    return $post;
});

Route::get('/updateloquent', function(){
    $post = Post::where('id', 2)->where('is_admin', 0)->update(['title'=>'New PHP Title', 'content'=>'Yes, I did it.']);
    return $post;
});

//Delete using Eloquent
Route::get('/delelo', function(){
    $post = Post::find(4)->delete();
});

//Delete bulk records
Route::get('/delbulk', function(){
    $post = Post::destroy([3,5,6]);
    return $post;
});

//Soft Deleting
Route::get('/softdel', function(){
    $post = Post::where('id', 11)->delete();
    return $post;
});

//Read the soft deleted object
Route::get('/readsoftdel', function(){
    $post = Post::onlyTrashed()->where('is_admin', 0)->get();
    return $post;
});

//Read all soft delete records
Route::get('/allsoftdel', function(){
    $post = Post::withTrashed()->where('is_admin', 0)->get();
//    echo"<pre>";
//    print_r($post);
//    echo '</pre>';
    return $post;
});

//Restore the deleted items
Route::get('/restore', function(){
    $post = Post::onlyTrashed()->where('is_admin', 0)->restore();
    return $post;
});

//Delete alternate item
Route::get('/alt', function(){
//    $i = 1;
//    for($z = 1/2; $z<=40; $z++){
//        $post = Post::find($z)->delete();
//    }
    $post = Post::where('id', 19)->delete();
    return $post;
});

//Delete Permanantly an item
Route::get('/forcedel', function(){
    $post = Post::onlyTrashed()->where('is_admin', 0)->forceDelete();
});

//Adding user to the Users Table
Route::get('/adduser', function(){
    $user = User::create(['name'=>'Zigana', 'email'=>'zigana@datamonsterpk.com', 'password'=>'123456', 'country_id' => 6]);
    return $user;
});

/*
|--------------------------------------------------------------------------
| ELOQUENT RELATIONSHIPS
|--------------------------------------------------------------------------
*/
// One to One Relationship
Route::get('/user/{id}/post', function($id){
    $userposts = User::find($id)->post;
    return $userposts;
});

//Inverse Relation
Route::get('/post/{id}/user', function($id){
    return Post::find($id)->user->email;
});

// One to Many Relationship
Route::get('/{id}/allposts', function($id){
    $user = User::find($id);
    foreach ($user->posts as $post) {
        echo "<h3>" . $post->title . "</h3>";
        echo "<p>" . $post->content . "</p>";
    }
});

// Many to Many Relationship
Route::get('/user/{id}/role', function($id){
//    $role = User::find($id)->roles;
//    
//    foreach($role as $user_role){
//        echo $user_role->name;
//    }
    $user = User::find($id)->roles->sortByDesc('name');
    return $user;
    
});

//Many to Many Relationship using role model
Route::get('/role/{id}/user', function($id){
    $role = Role::find($id)->users()->get();
    foreach($role as $user_role){
        echo $user_role->name;
    }
//    return $role->name;
});

// Accessing the Intermediate Table or Pivot Table
Route::get('/user/pivot', function(){
    $user = User::find(1);
    foreach($user->roles as $role){
        //echo $role->pivot->created_at;
        return $role->pivot;
    }
});

// Adding some Countries

Route::get('/addcountry', function(){
    $country = Country::create(['name'=>'United Kingdom']);
    return $country;
});

// Has Many Through Relationships
Route::get('/user/country', function(){
    $country = Country::find(4);
    foreach($country->posts as $post){
        echo $post->title . "<br/>";
    }
});

//Polymorphic Relations
Route::get('user/{id}/photos', function($id){
    $user = User::find($id);
    foreach($user->photos as $photo){
        echo $photo->path . "<br/>";
    }
});

Route::get('post/{id}/photos', function($id){
    $post = Post::find($id);
    if(!empty($post->photos)){
        foreach($post->photos as $photo){
            echo $photo . "<br/>";
        }
    }else{
        echo "This post id: $id don't have any photos attached.";
    }
});

// Polymorphic Relation the Inverse
Route::get('photo/{id}/post', function($id){
    $photo = Photo::findOrFail($id);
    return $photo->imageable;
});

// Polymorphic Relation Many to Many
Route::get('/post/tag', function(){
    $post = Post::findOrFail(1);
    //return $post;
    foreach($post->tags as $tag){
        return $tag->name . "<br/>";
    }
});

Route::get('/tag/post', function(){
    $tag = Tag::findOrFail(2);
    //return $tag;
    foreach($tag->posts as $post){
        return $post->title;
    }
});