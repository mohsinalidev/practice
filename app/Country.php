<?php

namespace DataMonster;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $fillable = ['name'];
    
    public function posts(){
        return $this->hasManyThrough('DataMonster\Post', 'DataMonster\User');
    }
}
