<?php

namespace DataMonster;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    //Eloquent modeling
    /*
     * For Soft Delete we use the existing methods of SoftDelete
     */
    
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];


    /*
    If the table name is different then declare it like this:
     * protected $table = 'posts';
     * protected $primaryKey = 'id';
    */
   protected $fillable = [
       'title',
       'content',
   ];
   
   public function user(){
       return $this->belongsTo('DataMonster\User');
   }
   
   public function photos(){
       return $this->morphMany('DataMonster\Photo', 'imageable');
   }
   
   public function tags(){
       return $this->morphToMany('DataMonster\Tag', 'taggable');
   }
}
