<?php

namespace DataMonster;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function posts(){
        return $this->morphedByMany('DataMonster\Post', 'taggable');
    }
    
    public function videos(){
        return $this->morphedByMany('DataMonster\Video', 'taggable');
    }
            
}
