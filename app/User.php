<?php

namespace DataMonster;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use DataMonster\Post;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'country_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function post()
    {
        return $this->hasOne('DataMonster\Post');
    }
    
    public function posts(){
        return $this->hasMany('DataMonster\Post');
    }
    
    public function roles(){
        return $this->belongsToMany('DataMonster\Role')->withPivot('created_at');
    }
    
    public function photos(){
        return $this->morphMany('DataMonster\Photo', 'imageable');
    }
}