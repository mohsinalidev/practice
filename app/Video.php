<?php

namespace DataMonster;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public function tags(){
        return $this->morphToMany('DataMonster\Tag', 'taggable');
    }
}
